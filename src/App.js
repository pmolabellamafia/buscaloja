import React, { useEffect, useState } from "react";
import "./App.css";

import Button from "@mui/material/Button";
import TextField from "@mui/material/TextField";

function App() {
  const initialValues = [
    {
      cd_cliente: null,
      cliente: null,
      endereco: null,
      cep: null,
      cidade: null,
      telefone: null,
      email: null,
      microrregiao: null,
      mesorregiao: null,
      uf: null,
      dist: null,
    },
  ];

  const [lojas, setLojas] = useState(initialValues);
  const [tracking, setTracking] = useState(null);
  const [checked, setChecked] = useState(false);
  const [cepOk, setCepOk] = useState(false);
  const [loading, setLoading] = useState(false);

  const submitHandler = async (e) => {
    e.preventDefault();

    const formData = new FormData(e.target);
    let { tracking } = Object.fromEntries(formData);
          
    let teste = tracking
    
    if(tracking.length === 8 && Number.isInteger(parseInt(tracking))){
      tracking = teste.substring(0,5) + "-" + teste.substring(5,8)
    }
    
    setTracking(tracking);
  };

  useEffect(() => {
    setChecked(false);
    async function fetchAPI() {
      setLoading(true);
      if (tracking === null || tracking === "") {
        setTracking(null);
        return;
      }

      await fetch(`https://busca-loja.herokuapp.com/api/tracking/${tracking}`)
        .then((response) => {
          if (response.ok) {
            setCepOk(true);
            return response.json()
          } else {
            throw new Error('Something went wrong');
          }
        })
        .then((data) => {
          setLojas(data);
          return data;
        })
        .catch((error) => {
          setCepOk(false);
          setLojas(null);
          console.log(error);
        });
      setChecked(true);
      setLoading(false);
    }
    fetchAPI();
  }, [tracking]);

  return (
    <div className="container">
      <div className="seachWrapper">
      <h3>Insira o CEP para pesquisar a loja mais próxima</h3>
      <form onSubmit={submitHandler}>
        <div>
          <TextField
            inputProps={{min: 0, style: { textAlign: 'center'}}}
            size="small"
            variant="standard"
            type="text"
            name="tracking"
            placeholder="00000-000"
          />
        </div>
        <div className="btnSerchWrapper">
          <Button
            sx={{ mt: 1 }}
            size="small"
            variant="outlined"
            type="submit"
            name="trackBtn"
            id="trackBtn"
          >
            Buscar
          </Button>
        </div>
      </form>
      </div>
      {checked ? (
        cepOk && lojas ? (
          lojas.map((ren) => (
            <>
              <div key={ren.cd_cliente} className="lojasContent">
                <h4>{ren.cidade}</h4>
                <p>
                  <b>Loja:</b> {ren.cliente}
                </p>
                <p>
                  <b>Endereço:</b> {ren.endereco}
                </p>
                <p>
                  <b>Telefone:</b> {ren.telefone}
                </p>
                <p>
                  <b>E-mail:</b> {ren.email}
                </p>
                <p>
                  <b>Distância:</b> {ren.dist.toFixed(2)} km
                </p>
                <p>
                  <b>resposta da api:</b> {ren.local}
                </p>
              </div>
            </>
          ))
        ) : (
          <div>
            <span className="errorMsg">
              CEP inserido inválido ou não encontrado
            </span>
          </div>
        )
      ) : loading && tracking ? (
        <>
          <p className="cepContent">Verificando CEP...</p>
        </>
      ) : (
        <>
          <br />
        </>
      )}
    </div>
  );
}

export default App;
